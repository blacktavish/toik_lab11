package com.example.demo.dto;

import lombok.*;
import java.io.Serializable;

@Getter
@NoArgsConstructor
@AllArgsConstructor

public class MovieDto implements Serializable {
    private int movieId;
    private String title;
    private int year;
    private String image;
}
